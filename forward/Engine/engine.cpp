#include "engine.h"
#include <iostream>
#include <cmath>

Engine::Engine(int temp = 20)
{
    tSr = temp;
    T = temp;
}

/**
 * @brief Увеличивает скорость вращения
 *
 */
void Engine::incV() { this->currentV += getA(); }

/**
 * @brief Увеличивает температуру двигателя
 *
 */
void Engine::incT()
{
    this->T -= C * (this->tSr - this->T);
    this->T += getM() * hM + pow(currentV, 2) * hV;
}

/**
 * @brief Возвращает текущее ускорение
 *
 * @return int
 */
double Engine::getA() { return this->I * getM(); }

/**
 * @brief Возвращает текущий крутящий момент
 *
 * @return int
 */
int Engine::getM()
{
    int i = 0;
    while (this->currentV > V[i] && i < V.size())
    {
        i++;
    }

    return this->M[i];
}

/**
 * @brief Задает температуру окружающей среды
 * 
 */
void Engine::setTemp(int temp)
{
    this->T = temp;
    this->tSr = temp;
}

/**
 * @brief Симуляция ДВС
 * 
 */
void Engine::simulate()
{
    // Посекундная симуляция работы
    while (this->T < this->tMax)
    {
        incV();
        incT();
        this->time++;
    }
}

/**
 * @brief Запускает тестовый стенд
 * 
 * @return int Время в секундах с момента старта до перегрева
 */
int Engine::test()
{
    this->simulate();

    return this->time;
}