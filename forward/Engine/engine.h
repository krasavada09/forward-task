#ifndef ENGINE_H
#define ENGINE_H

#include <vector>

using namespace std;

class Engine
{
public:
    Engine(int temp);

private:
    const double I = 0.1;                            // Момент инерции
    const vector<int> M = {20, 75, 100, 105, 75, 0}; // Таблица крутящего момента
    vector<int> V = {0, 75, 150, 200, 250, 300};     // Таблица скорости вращения коленвала

    double hM = 0.01;   // Скорость нагрева от крутящего момента
    double hV = 0.0001; // Скорость нагрева от вращения коленвала
    double C = 0.1;     // Скорость охлаждения от температуры двигателя и среды

    int tMax = 110; // Температура перегрева
    double T = 0;   // Температура двигателя
    int tSr;        // Температура среды

    double currentV = 0; // Текущая скорость вращения коленвала

    int time = 0; // Время работы двигателя

    void setTemp(int temp);

    void incV();
    void incT();

    double getA();
    int getM();

    void simulate();

public:
    int test();
};

#endif